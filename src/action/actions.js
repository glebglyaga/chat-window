import * as ActionTypes from '../model/action-types';
import * as MessageMode from '../model/message-mode';
import {
  getEditMessageIndex,
  getInputText,
  getMessageMode,
  getMessages,
} from '../model/selectors/selectors';

export function addMessage(text) {
  return {
    type:    ActionTypes.MESSAGE_DID_ADD,
    payload: text
  };
}

export function deleteMessage(index) {
  return (dispatch, getState) => {
    const editMessageIndex = getEditMessageIndex(getState());

    if (editMessageIndex > 0 && editMessageIndex > index) {
      dispatch(setEditMessageIndex(editMessageIndex - 1));
    }

    if (editMessageIndex === index) {
      dispatch(resetInput());
    }

    dispatch(deleteMessageByIndex(index));
  };
}

export function deleteMessageByIndex(index) {
  return {
    type:    ActionTypes.MESSAGE_DID_DELETE,
    payload: index
  };
}

export function editLastMessage() {
  return (dispatch, getState) => {
    const messages = getMessages(getState());

    dispatch(editMessage(messages.length - 1));
  };
}

export function editMessage(index) {
  return (dispatch, getState) => {
    const messages = getMessages(getState());

    dispatch(setMessageMode(MessageMode.EDITING));
    dispatch(setEditMessageIndex(index));
    dispatch(setInputText(messages[index]));
  };
}

export function resetInput() {
  return dispatch => {
    dispatch(setMessageMode(MessageMode.WRITING));
    dispatch(setEditMessageIndex(null));
    dispatch(setInputText(''));
  };
}

export function sendMessage() {
  return (dispatch, getState) => {
    const editMessageIndex = getEditMessageIndex(getState());
    const inputText = getInputText(getState());
    const mode = getMessageMode(getState());

    if (inputText.trim()) {
      if (mode === MessageMode.WRITING) {
        dispatch(addMessage(inputText));
      } else if (mode === MessageMode.EDITING) {
        dispatch(updateMessage(inputText, editMessageIndex));
      }

      dispatch(resetInput());
    }
  };
}

export function setError(error) {
  return {
    type:    ActionTypes.ERROR_DID_OCCUR,
    payload: error
  };
}

export function setEditMessageIndex(index) {
  return {
    type:    ActionTypes.EDIT_MESSAGE_INDEX_DID_CHANGE,
    payload: index
  };
}

export function setInputText(text) {
  return {
    type:    ActionTypes.INPUT_TEXT_DID_CHANGE,
    payload: text
  };
}

export function setMessageMode(mode) {
  return {
    type:    ActionTypes.MESSAGE_MODE_DID_CHANGE,
    payload: mode
  };
}

export function setMessages(messages) {
  return {
    type:    ActionTypes.MESSAGES_DID_CHANGE,
    payload: messages
  };
}

export function updateMessage(text, index) {
  return {
    type:    ActionTypes.MESSAGE_DID_CHANGE,
    payload: { text, index }
  };
}