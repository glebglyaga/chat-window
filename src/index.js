import ChatController from './controller/chat-controller';
import '../style/style.scss';

export function Main(config) {
  let appController = new ChatController(config);

  this.dispose = () => {
    if (appController !== null) {
      appController.dispose();
    }

    appController = null;
  };
}
