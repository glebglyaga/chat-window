import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';

import { createChatStore, getInitialState } from '../model/chat-store';
import Logger from '../util/logger';
import NetService from '../service/net-service';
import RootLayout from '../view/root-layout';
import { getError } from '../model/selectors/selectors';
import storeObserver from '../util/store-observer';
import {
  deleteMessage,
  editLastMessage,
  editMessage,
  sendMessage,
  setInputText,
  setMessages,
} from '../action/actions';

export default class ChatController {
  constructor({ view, messages }) {
    this.logger = Logger.createTaggedLoggers('ChatController');
    this.view = view;
    this.messages = messages;
    this.store = createChatStore(getInitialState());
    this.storeObservers = [];

    DEBUG && this.logger.log('App initialized with config:', { view, messages });

    this.addListeners();
    this.bootstrap();
    this.render();
  }

  addListeners() {
    this.addObserver(getError, error => {
      if (error) {
        this.logger.error('Error: ', error);
      }
    });
  }

  addObserver(selector, listener) {
    this.storeObservers.push(storeObserver(this.store, selector, listener));
  }

  bootstrap() {
    this.store.dispatch(setMessages(this.messages));
  }

  dispose() {
    if (this.storeObservers !== null) {
      this.storeObservers.forEach(unSub => unSub());
      this.storeObservers = null;
    }

    this.store = null;
  }

  render() {
    ReactDom.render(
      <Provider store={this.store}>
        <RootLayout
          onMessageDelete={index => this.deleteMessage(index)}
          onMessageEdit={index => this.editMessage(index)}
          onMessageSend={() => this.sendMessage()}
          onLastMessageEdit={() => this.lastMessageEdit()}
          rootViewDidMount={view => this.rootViewDidMount(view)}
          textInputDidChange={text => this.setTextInput(text)}
        />
      </Provider>,
      this.view
    );
  }

  sendMessage() {
    this.store.dispatch(sendMessage());
  }

  setTextInput(text) {
    this.store.dispatch(setInputText(text));
  }

  deleteMessage(index) {
    this.store.dispatch(deleteMessage(index));
  }

  editMessage(index) {
    this.store.dispatch(editMessage(index));
  }

  lastMessageEdit() {
    this.store.dispatch(editLastMessage());
  }

  rootViewDidMount(view) {
    DEBUG && this.logger.log('Root View did mount');
    this.rootView = view;
  }
}