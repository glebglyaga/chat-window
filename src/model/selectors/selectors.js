export const getError = state => state.error;
export const getInputText = state => state.inputText;
export const getEditMessageIndex = state => state.editMessageIndex;
export const getMessageMode = state => state.messageMode;
export const getMessages = state => state.messages;