export const ERROR_DID_OCCUR = 'errorDidOccur';
export const EDIT_MESSAGE_INDEX_DID_CHANGE = 'editMessageIndexDidChange';
export const INPUT_TEXT_DID_CHANGE = 'inputTextDidChange';
export const MESSAGE_DID_ADD = 'messageDidAdd';
export const MESSAGE_DID_CHANGE = 'messageDidChange';
export const MESSAGE_DID_DELETE = 'messageDidDelete';
export const MESSAGES_DID_CHANGE = 'messagesDidChange';
export const MESSAGE_MODE_DID_CHANGE = 'messageModeDidChange';