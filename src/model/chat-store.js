import { applyMiddleware, createStore } from 'redux';
import reduxThunkMiddleware from 'redux-thunk';

import * as ActionTypes from '../model/action-types';
import FsaReducer from '../util/fsa-reducer';
import * as MessageMode from './message-mode';
import * as Reducers from './reducers';

export function createChatStore(initialState) {
  const storeWithMiddleware = applyMiddleware(reduxThunkMiddleware)(createStore);
  const reducer = new FsaReducer(Reducers, ActionTypes).create();

  return storeWithMiddleware(reducer, initialState);
}

export function getInitialState() {
  let initialScheme = {
    error:            null,
    editMessageIndex: null,
    inputText:        '',
    messageMode:      MessageMode.WRITING,
    messages:         [],
  };

  return initialScheme;
}
