import * as ActionTypes from './action-types';

export function error(state, action) {
  switch (action.type) {
    case ActionTypes.ERROR_DID_OCCUR:
      return action.payload;
    default:
      return state;
  }
}

export function editMessageIndex(state, action) {
  switch (action.type) {
    case ActionTypes.EDIT_MESSAGE_INDEX_DID_CHANGE:
      return action.payload;
    default:
      return state;
  }
}

export function inputText(state, action) {
  switch (action.type) {
    case ActionTypes.INPUT_TEXT_DID_CHANGE:
      return action.payload;
    default:
      return state;
  }
}

export function messageMode(state, action) {
  switch (action.type) {
    case ActionTypes.MESSAGE_MODE_DID_CHANGE:
      return action.payload;
    default:
      return state;
  }
}

export function messages(state, action) {
  let messages;

  switch (action.type) {
    case ActionTypes.MESSAGE_DID_ADD:
      return [ ...state, action.payload ];
    case ActionTypes.MESSAGE_DID_CHANGE:
      messages = [ ...state ];
      messages.splice(action.payload.index, 1, action.payload.text);

      return messages;
    case ActionTypes.MESSAGE_DID_DELETE:
      messages = [ ...state ];
      messages.splice(action.payload, 1);

      return messages;
    case ActionTypes.MESSAGES_DID_CHANGE:
      return action.payload;
    default:
      return state;
  }
}
