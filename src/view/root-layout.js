import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';

import TextInput from './display/text-input';
import MessagesContainer from './display/messages-container';
import {
  getInputText,
  getMessages,
} from '../model/selectors/selectors';
import MessagesCounter from './display/messages-counter';

class RootLayout extends React.Component {
  componentDidMount() {
    this.props.rootViewDidMount(this.view);
  }

  render() {
    return (
      <div className="root-layout" ref={root => this.view = root}>
        <MessagesCounter count={this.props.messages.length}/>
        <MessagesContainer
          messages={this.props.messages}
          onMessageDelete={this.props.onMessageDelete}
          onMessageEdit={this.props.onMessageEdit}
        />
        <TextInput
          text={this.props.inputText}
          onChange={this.props.textInputDidChange}
          onSend={this.props.onMessageSend}
          onEdit={this.props.onLastMessageEdit}
        />
      </div>
    );
  }
}

export default connect(state => {
  return {
    inputText: getInputText(state),
    messages:  getMessages(state),
  };
})(RootLayout);

RootLayout.propTypes = {
  inputText:          PropTypes.string,
  messages:           PropTypes.arrayOf(PropTypes.string),
  onLastMessageEdit:  PropTypes.func,
  onMessageDelete:    PropTypes.func,
  onMessageEdit:      PropTypes.func,
  onMessageSend:      PropTypes.func,
  rootViewDidMount:   PropTypes.func.isRequired,
  textInputDidChange: PropTypes.func,
};