import PropTypes from 'prop-types';
import React from 'react';

import MessageItem from './message-item';

export default function MessagesContainer({ messages, onMessageEdit, onMessageDelete }){
  return (
    <div className="messages-container">
      {messages.map((message, index) => {
        return (
          <div key={index}>
            <MessageItem
              text={message}
              index={index}
              onEdit={() => onMessageEdit(index)}
              onDelete={() => onMessageDelete(index)}
            />
          </div>
        );
      })}
    </div>
  );
}

MessagesContainer.propTypes = {
  messages:        PropTypes.arrayOf(PropTypes.string),
  onMessageEdit:   PropTypes.func,
  onMessageDelete: PropTypes.func,
};