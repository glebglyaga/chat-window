import PropTypes from 'prop-types';
import React from 'react';

export default class TextInput extends React.Component {
  constructor(props) {
    super(props);

    this.textInputRef = null;
  }

  componentDidUpdate() {
    this.textInputRef.focus();
  }

  keyDownHandler(e) {
    if (e.keyCode === 13) {
      this.props.onSend();
    } else if (e.keyCode === 38) {
      this.props.onEdit();
    }
  }

  render() {
    return (
      <div className="text-input">
        <input
          type="input"
          value={this.props.text}
          autoComplete="off"
          autoFocus={true}
          ref={input => this.textInputRef = input}
          onChange={() => this.props.onChange(this.textInputRef.value)}
          onKeyDown={e => this.keyDownHandler(e)}
        />
      </div>
    );
  }
}

TextInput.propTypes = {
  text:     PropTypes.string,
  onEdit:   PropTypes.func,
  onChange: PropTypes.func,
  onSend:   PropTypes.func,
};

TextInput.defaultProps = {
  text: '',
};