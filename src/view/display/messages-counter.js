import PropTypes from 'prop-types';
import React from 'react';

export default function MessagesCounter({ count }) {
  return (
    <div className="messages-counter">{count}</div>
  );
}

MessagesCounter.propTypes = {
  count: PropTypes.number,
};