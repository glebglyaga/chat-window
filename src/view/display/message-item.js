import PropTypes from 'prop-types';
import React from 'react';

export default function MessageItem({ text, onEdit, onDelete }) {
  return (
    <div className='message-item'>
      <div className='message-item__controls'>
        <span onClick={onEdit}>Edit</span> <span onClick={onDelete}>X</span>
      </div>
      <div className='message-item__text'>{text}</div>
    </div>
  );
}

MessageItem.propTypes = {
  text:     PropTypes.string,
  onEdit:   PropTypes.func,
  onDelete: PropTypes.func,
};