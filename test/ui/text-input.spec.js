import { expect } from 'chai';
import { mount } from 'enzyme';
import React from 'react';
import { spy } from 'sinon';

import TextInput from '../../src/view/display/text-input';

describe('Text Input', () => {
  let wrapper;

  it('renders default text', () => {
    wrapper = mount(<TextInput/>);

    expect(wrapper.find('input').prop('value')).to.equal('');
  });

  it('renders specific text', () => {
    wrapper = mount(<TextInput text='initial value'/>);

    expect(wrapper.find('input').prop('value')).to.equal('initial value');
  });

  it('invokes onChange handler', () => {
    const onChangeSpy = spy();

    wrapper = mount(<TextInput onChange={onChangeSpy} />);
    wrapper.setProps({ text: 'someinput' });
    wrapper.find('input').simulate('change');

    expect(onChangeSpy.lastCall.args[0]).to.be.equal('someinput');
  });

  it('invokes onEdit handler when press arrow up', () => {
    const onEditSpy = spy();

    wrapper = mount(<TextInput onEdit={onEditSpy} />);
    wrapper.find('input').simulate('keyDown', { keyCode: 38 });

    expect(onEditSpy).called;
  });

  it('invokes onSend handler', () => {
    const onSendSpy = spy();

    wrapper = mount(<TextInput onSend={onSendSpy} />);
    wrapper.find('input').simulate('keyDown', { keyCode: 13 });

    expect(onSendSpy).called;
  });
});