import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import React from 'react';
import Sinon from 'sinon';

import MessageItem from '../../src/view/display/message-item';
import MessagesContainer from '../../src/view/display/messages-container';

describe('Messages Container', () => {
  it('renders items', () => {
    const wrapper = shallow(<MessagesContainer messages={[ 'message 1', 'message 2', 'message 3' ]} />);

    expect(wrapper.find(MessageItem)).to.have.lengthOf(3);
  });

  it('invokes onMessageEdit handler', () => {
    const onMessageEditSpy = Sinon.spy();
    const wrapper = mount(
      <MessagesContainer
        messages={[ 'message 1', 'message 2', 'message 3' ]}
        onMessageEdit={onMessageEditSpy}
      />
    );

    wrapper.find('.message-item__controls').at(2).childAt(0).simulate('click');

    expect(onMessageEditSpy).calledWith(2);
  });

  it('invokes onMessageDelete handler', () => {
    const onMessageDeleteSpy = Sinon.spy();
    const wrapper = mount(
      <MessagesContainer
        messages={[ 'message 1', 'message 2', 'message 3' ]}
        onMessageDelete={onMessageDeleteSpy}
      />
    );

    wrapper.find('.message-item__controls').at(1).childAt(1).simulate('click');

    expect(onMessageDeleteSpy).calledWith(1);
  });
});
