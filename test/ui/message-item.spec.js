import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import React from 'react';
import Sinon from 'sinon';

import MessageItem from '../../src/view/display/message-item';

describe('Message Item', () => {
  it('renders text', () => {
    const wrapper = shallow(<MessageItem text="some text"/>);

    expect(wrapper.find('.message-item__text')).to.have.text('some text');
  });

  it('invokes onEdit handler', () => {
    const onEditSpy = Sinon.spy();
    const wrapper = mount(<MessageItem onEdit={onEditSpy}/>);

    wrapper.find('.message-item__controls').childAt(0).simulate('click');

    expect(onEditSpy).called;
  });

  it('invokes onDelete handler', () => {
    const onDeleteSpy = Sinon.spy();
    const wrapper = mount(<MessageItem onDelete={onDeleteSpy} />);

    wrapper.find('.message-item__controls').childAt(1).simulate('click');

    expect(onDeleteSpy).called;
  });
});
