import { expect } from 'chai';
import { shallow } from 'enzyme';
import React from 'react';

import MessagesCounter from '../../src/view/display/messages-counter';

describe('Messages Counter', () => {
  it('renders messages counter', () => {
    const wrapper = shallow(<MessagesCounter count={4} />);

    expect(wrapper.find('.messages-counter')).to.have.text('4');
  });
});
