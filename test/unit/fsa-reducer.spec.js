import { expect } from 'chai';
import Sinon from 'sinon';

import FsaReducer from '../../src/util/fsa-reducer';

describe('Flux Standard Action Reducer', () => {
  let actions;
  let reducerSingle, reducerMultiple, reducerEmpty;
  let initialState;

  before(() => {
    actions = { RUN: 'run', WALK: 'walk', SWIM: 'swim', JUMP: 'jump' };
    reducerSingle = (state, action) => {
      switch (action.type) {
        case actions.RUN:
          return 1;
        default:
          return state;
      }
    };
    reducerMultiple = (state, action) => {
      switch (action.type) {
        case actions.WALK:
          return 2;
        case actions.SWIM:
          return 3;
        case actions.JUMP:
          return 4;
        default:
          return state;
      }
    };
    reducerEmpty = (state, action) => state;
    initialState = {
      reducerSingle:   null,
      reducerMultiple: null
    };
  });

  it('maps single action', () => {
    let reducer = new FsaReducer({ reducerSingle }, actions).create();
    expect(reducer(initialState, { type: actions.RUN })).to.have.property('reducerSingle', 1);
  });

  it('maps multiple actions', () => {
    let reducer = new FsaReducer({ reducerSingle, reducerMultiple }, actions).create();
    expect(reducer(initialState, { type: actions.JUMP })).to.have.property('reducerMultiple', 4);
  });

  it('uses reducer name for the state slices', () => {
    let reducer = new FsaReducer({ reducerSingle, reducerMultiple }, actions).create();
    let state = reducer({}, { type: actions.JUMP });
    expect(state).to.have.property('reducerMultiple');
  });

  it('constant values stay in the state', () => {
    let reducer = new FsaReducer({ reducerSingle, reducerMultiple }, actions).create();
    expect(reducer(Object.assign({}, initialState, { demo: 'awesome' }), { type: actions.RUN })).to.have.property('demo', 'awesome');
  });

  it('creates new instance of the state', () => {
    let reducer = new FsaReducer({ reducerSingle }, actions).create();
    expect(reducer(initialState, { type: actions.RUN }) === initialState).to.be.false;
  });

  it('returns the same state for no changes', () => {
    let reducer = new FsaReducer({ reducerSingle }, actions).create();
    expect(reducer(initialState, { type: 'extra' }) === initialState).to.be.true;
  });

  it('omits reducers without actions', () => {
    let reducer = new FsaReducer({ reducerEmpty }, actions).create();
    expect(reducer(initialState, { type: actions.WALK })).to.not.have.property('reducerEmpty');
  });

  it('invokes only the target slice', () => {
    let init = true;
    let spy = Sinon.spy();
    let reducerInit = (state, action) => {
      if (init === false) {
        spy();
      }
      switch (action) {
        case 'demo':
          return 800;
        default:
          return state;
      }
    };
    let extraActions = Object.assign({}, actions, { DEMO: 'demo' });
    let reducer = new FsaReducer({ reducerInit, reducerSingle, reducerMultiple }, extraActions).create();
    init = false;
    reducer(initialState, { type: actions.RUN });
    expect(spy.called).to.be.false;
  });

});
