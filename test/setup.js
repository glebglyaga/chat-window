import Chai from 'chai';
import ChaiEnzyme from 'chai-enzyme';
import SinonChai from 'sinon-chai';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { JSDOM } from 'jsdom';

const dom = new JSDOM('', { url: 'https://localhost/' });
const exposedProperties = [ 'window' ];

Chai.use(ChaiEnzyme());
Chai.use(SinonChai);
Enzyme.configure({ adapter: new Adapter() });

global.window = dom.window;
global.DEBUG = false;

Object.keys(dom.window).forEach(property => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);

    global[property] = dom.window[property];
  }
});
